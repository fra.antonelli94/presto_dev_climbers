<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Public controller

Route::get('/', 'FrontController@index')->name('home');
Route::get('/who', 'FrontController@who')->name('front.who');




Route::middleware(['auth'])->group(function () {

Route::get('/announcements/new', 'AnnouncementController@createAnnouncement')->name('announcement.create');
Route::post('/announcements', 'AnnouncementController@storeAnnouncement')->name('announcement.store');

});

// User Controller

Route::get('/user/home', 'UserController@index')->name('user.home');
Route::get('/contacts/workwithus', 'UserController@workwithus')->name('contacts.workwithus');
Route::post('/contacts/workwithus', 'UserController@workwithus_ok')->name('contacts.workwithusok');
Route::get('/revisor/request_ok', 'UserController@request_ok')->name('revisor.requestok');


//Announcements Controller

Route::get('/announcements/{id}', 'AnnouncementController@detail')->name('announcement.detail')->where('id', '[0-9]+');

Route::get('/announcements/show', 'AnnouncementController@show')->name('announcement.show');

Route::get('/announcements/{announcement_id}/edit', 'AnnouncementController@edit')->name('announcement.edit');

Route::put('/announcements/{announcement_id}/update', 'AnnouncementController@update')->name('announcement.update');

Route::post('/announcements/{announcement_id}/images', 'AnnouncementController@storeImage')->name('announcement.store.image');

Route::get('/announcements/{announcement_id}/images', 'AnnouncementController@getImages')->name('announcement.get.images');

Route::delete('/announcements/{announcement_id}/images/{announcement_image_id}', 'AnnouncementController@deleteImage')->name('announcement.delete.image');



//Revisor Controller

Route::get('/revisor/home', 'RevisorController@index')->name('revisor.home');
Route::post('/revisor/announcements/{id}/accept', 'RevisorController@accept')->name('revisor.accept');
Route::post('/revisor/announcements/{id}/reject', 'RevisorController@reject')->name('revisor.reject');



//auth routes
Auth::routes();

//category controller
Route::get('/categories/{id}/announcements','CategoryController@category')->name('announcement.category');


