<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Announcement extends Model
{
    protected $fillable =  ['title', 'body', 'category_id', 'price'];
    
    public function category()
	{
		return $this->belongsTo('App\Category');
	}
	public function user()
	{
		return $this->belongsTo('App\User');
	}

	public function images()
	{
		return $this->hasMany('App\AnnouncementImage');
	}

 }
