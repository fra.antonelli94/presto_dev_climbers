<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Announcement;
use App\Category;

class FrontController extends Controller
{
    public function index()
    {
    	$announcements = Announcement::where('accepted', true)->orderBy('id','desc')->take(6)->get();
    	$categories = Category::all();
    	
    	return view('front.index', compact('announcements','categories'));
    }


    public function who()
    {
    	return view('front.who');
    }
}
