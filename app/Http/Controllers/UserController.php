<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;
use App\Mail\RequestReceived; 
use Illuminate\Support\Facades\Mail;



class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        return view('home');
    }

    public function workwithus()
    {
    	return view('contacts.workwithus');
    }

	public function workwithus_ok(Request $request )
	{
        $name = $request->input('name');
        $email = $request->input('email');
        $description = $request->input('description');
        $cv = $request->input('cv');

		//dd ($name, $email, $description, $cv);

		//Invio e-mail

        Mail::to('intontisonia@live.it')->send(
            new RequestReceived ($name, $email, $description, $cv)
        );

		// return redirect ('')
        return redirect(route('revisor.requestok'));
	}

    public function request_ok()
    {
        return view('revisor.requestok');
    }

}
