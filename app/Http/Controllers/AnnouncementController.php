<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Category;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

use App\Http\Requests\AnnouncementRequest;
use App\Announcement;
use App\AnnouncementImage;
use App\Jobs\AnnouncementImageResizeJob;
use App\Jobs\DetectInappropriateImageJob;

class AnnouncementController extends Controller
{
    public function createAnnouncement()
    {
        $categories = Category::all();
        return view('announcement.create', ['categories' => $categories]);
    }


    public function storeAnnouncement(AnnouncementRequest $request)
    {
        $a = new Announcement;
      
        $a->title = $request->input('title');
        $a->description = $request->input('description');
        $a->category_id = $request->input('category');
        $a->user_id = Auth::user()->id;
        $a->price = $request->input('price');

        $a->save();

    	return redirect(route('announcement.edit', [$a->id]))->with('success', 'announcement.stored')->withInput();

    }


    public function detail($id)
    {
        $announcement = Announcement::find($id);
        return view('announcement.details', ['announcement'=>$announcement]);
    }

    public function show()
    {
        $categories = Category::all();
        $announcements = Announcement::where('accepted', true)->orderBy('created_at','desc')->paginate(6);


        // return view('announcement.show', ['announcements'=>$announcements, 'categories'=> $categories]);
        
        return view('announcement.show', compact('announcements', 'categories'));
    }


    public function storeImage(Request $req, $announcement_id)
    {
        $file = $req->file('file');
        $store = $file->store("public/announcements/$announcement_id");
        $src = Storage::url($store);

        $ai = new AnnouncementImage;
        $ai->announcement_id = $announcement_id;
        $ai->src = $src;
        $ai->mimetype = $file->getMimeType();
        $ai->size = $file->getSize();
        $ai->save();

        dispatch(new DetectInappropriateImageJob($src, $ai->id));;

        //dispatch(new AnnouncementImageResizeJob($src));
        AnnouncementImageResizeJob::dispatch($src);

        return response()->json(['status' => 'success','id' => $ai->id], 200);
    }

    public function edit($announcement_id)
    {
        $categories = Category::all();
        $announcement = Announcement::find($announcement_id);
        return view('announcement.edit', compact('announcement', 'categories'));
    }
    
    public function update(AnnouncementRequest $request, $announcement_id)
    {
        $announcement = Announcement::find($announcement_id);
        $announcement->update($request->input());
        $announcement->save();
        return redirect(route('announcement.edit', [$announcement->id]))->with('success', 'announcement.edited');    
    }

    public function getImages($announcement_id){
        $announcement = Announcement::find($announcement_id);

        $images = $announcement->images()->get();

        $response = [];
        foreach ($images as $image) {
            $response[] = [
                'id' => $image->id,
                'src' => $image->src,
                'size' => $image->size
            ];
        }

        return response()->json($response, 200);

    }

    public function deleteImage($announcement_id, $announcement_image_id){
        AnnouncementImage::destroy($announcement_image_id);
    }
}
