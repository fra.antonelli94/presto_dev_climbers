<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Announcement;
use App\Category;

class CategoryController extends Controller
{
   public function category_old($category_id)
   {
   	 $category = Category::with(['announcements','announcements.user'])->find($category_id);
   	 
   	 return view('announcement.category',compact('category'));
   }

    public function category($category_id)
   {
   	 $categories = Category::all();
     $announcements = Announcement::where('category_id',$category_id)->where('accepted', true)->orderBy('created_at','desc')->paginate(6);
     
     return view('front.index', compact('announcements', 'categories', 'category_id'));

   }
}
