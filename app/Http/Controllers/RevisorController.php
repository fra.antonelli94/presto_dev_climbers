<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Announcement;

class RevisorController extends Controller
{
    public function __construct()
    {
    	$this->middleware('auth.revisor');
    }

    public function index()
    {
    	$announcements = Announcement::where('accepted', null)->paginate(1);
    	return view('revisor.home', compact('announcements'));
    }

    public function accept($announcement_id)
    {
    	$announcement = Announcement::find($announcement_id);
    	$announcement->accepted = true;
    	$announcement->save();
    	return redirect(route('revisor.home'));
    }

    public function reject($announcement_id)
    {
    	$announcement = Announcement::find($announcement_id);
    	$announcement->accepted = false;
    	$announcement->save();
    	return redirect(route('revisor.home'));
    }
}

