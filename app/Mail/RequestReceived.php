<?php
namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;



class RequestReceived extends Mailable

{
    use Queueable, SerializesModels;

    private $bag;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name, $email, $description, $cv)
    {
        $this->bag = [
            'name'=>$name,
            'email'=>$email,
            'description'=>$description,
            'cv'=>$cv,

        ];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.request', $this->bag);
    }
}
