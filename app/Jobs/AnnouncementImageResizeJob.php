<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Folklore\Image\Facades\Image;
use Imagine\Image\Box;

class AnnouncementImageResizeJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $src;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($src)
    {
        $this->src = $src;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $imageAbsolutePathFile = public_path($this->src);

        if(!file_exists($imageAbsolutePathFile))
            return;

        $newImage = Image::make($imageAbsolutePathFile, [
            'width' => 300,
            'height' => 300,
            'crop' => true,
        ]);

        //per fare il resize senza crop
        // $newImage = Image::open($imageAbsolutePathFile);
        // $newImage->resize(new Box(640, 400));


        $watermark = Image::open(public_path('/watermark.png')); //Salvare un immagine in 
        $size      = $newImage->getSize();
        $wSize      = $watermark->getSize();

        $bottomRight = new \Imagine\Image\Point(
            ($size->getWidth() /2)-$wSize->getWidth()/2,
            ($size->getHeight() /2)-$wSize->getHeight()/2

            //fatto da Nico!!

        );

        $newImage->paste($watermark, $bottomRight);

        $newImage->save($imageAbsolutePathFile);

    }
}
