<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;

class DetectInappropriateImageJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $src_image;
    protected $announcementImage_id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($src_image, $announcementImage_id)
    {
        $this->src_image = $src_image;
        $this->announcementImage_id = $announcementImage_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Log::alert('detect in corso '.$this->src_image);

        // #1 leggere le credenziali di google vision API

        $google_key = config('app.googleVisionApiKey');
        Log::alert('google vision api key: '.$google_key);

        
        // #2 codificare l'immagine in base64

        $percorso_assoluto = public_path($this->src_image);
        Log::alert('percorso_assoluto: '.$percorso_assoluto);

        if(!file_exists($percorso_assoluto)){

            Log::alert('immagine inesistente: ');
            return;
        }
        $b64image = base64_encode(file_get_contents($percorso_assoluto)); 
        Log::alert('b64image: '.$b64image);


        // #3 formulare una richiesta specifica a google vision api

        $requestBody = [
            "requests" => [
                "features" => [
                    ["type" => 'SAFE_SEARCH_DETECTION'],
                    ["type" =>'LABEL_DETECTION'],

                ],
                "image" => [
                        "content" => $b64image
                ]
            ]

        ];
 
        // #4 inviare la richiesta all'API d google vision 

        $requestBodyJson = json_encode($requestBody);

        $url = "https://vision.googleapis.com/v1/images:annotate?key=$google_key"; 

        $client = new \GuzzleHttp\Client([ 'verify' => false]);

        $response = $client->request('POST', $url, [
            'body'=> $requestBodyJson
        ]);

        $body = $response->getBody();
        $stringBody = (string) $body;

        Log::alert('risposta: ' .$stringBody);

        // #5 recuperare il risultato 

        $response = json_decode($stringBody);


        $labels = $response->responses[0]->labelAnnotations;
        $safe = $response->responses[0]->safeSearchAnnotation;

        $strLabels = '';

        foreach($labels as $label){
            $strLabels = $strLabels ."," .$label->description;     
        }

        Log::alert('labels: '. $strLabels);

        $adult = $safe->adult;
        $spoof = $safe->spoof;
        $medical = $safe->medical;
        $violence = $safe->violence;
        $racy = $safe->racy;

        Log::alert('safe: adult '. $adult);
        Log::alert('safe: spoof '. $spoof);
        Log::alert('safe: medical '. $medical);
        Log::alert('safe: violence '. $violence);
        Log::alert('safe: racy '. $racy);

        // #6 memorizzare il risultato in announcementImages

        $i = \App\AnnouncementImage::find($this->announcementImage_id);

        $i->labels = $strLabels;
        $i->adult = $adult;
        $i->spoof = $spoof;
        $i->medical = $medical;
        $i->violence = $violence;
        $i->racy = $racy;
        

        $i->save();

    }
}
