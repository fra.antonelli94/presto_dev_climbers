<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class MakeUserRevisor extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'presto:makeRevisor';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Rende un utente Revisor';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $email = $this->ask("Inserisci la mail dell'utente che vuoi rendere Revisor");
        
        $u = \App\User::where('email', $email)->first();
        if (!$u){
            $this->error('Utente non trovato');
            return;
        }


        $u->revisor =true;

        $u->save();
        $this->info('Utente abilitato a Revisor');
         

    }
}
