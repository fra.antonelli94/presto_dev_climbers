<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Category;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
     Schema::create('categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->timestamps();
        });
        $categories = [
           'Giochi',
           'Auto',
           'Moto',
           'Elettrodomestici',
           'Immobili',
           'Elettronica',
           'Vintage',
           'Giardinaggio',
           'Libri'
        ];


 // Metodo ORM 
        foreach ($categories as $category){
            $c = new Category();
            $c->name = $category;
            $c->save();
            }



        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
