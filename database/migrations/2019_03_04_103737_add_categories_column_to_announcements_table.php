<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCategoriesColumnToAnnouncementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
        public function up()
    {
        Schema::table('announcements', function (Blueprint $table) {

            $table->unsignedBigInteger('category_id')->after('price');

            $table->foreign('category_id')->references('id')->on('categories');
            

        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('announcements', function (Blueprint $table) {

            $table->dropForeign(['category_id']);

            $table->dropColumn('category_id');

        });
    }
}
