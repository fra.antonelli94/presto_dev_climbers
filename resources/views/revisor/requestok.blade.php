@extends('layouts.app')


@section('content')
<div class="container justify-content-center">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card justify-content-center">

                <div class="card-body justify-content-center">

                    <h3>Grazie per averci contattato, la tua richiesta sarà valutata da un amministratore.</h3>
               
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
