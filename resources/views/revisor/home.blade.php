@extends('layouts.app')



@section('content')

@if (count($announcements) > 0)


@foreach($announcements as $announcement)
<div class="container justify-content-center">
   <div class="row justify-content-center">
    <div class="col-md-8">
       <div class="card justify-content-center">
        <div class="card-header text-center"><h3>Benvenuto REVISORE {{Auth::user()->name}}</h3></div>

        <div class="card-body justify-content-center">

            <h3 class="text-center">Annuncio da revisionare</h3>

            <br>
            <div class="row justify-content-center">
                <div class="col-md-12 col-lg-12 col-sm-12 col-xl-12">
                    <div class="card-deck">
                        <br>
                        <div class="card border-dark animated slideInUp">
                           <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                              <div class="carousel-inner">
                                @if(count($announcement->images) == 0)
                                  <img src="/img/image_not_found.png" class="d-block w-50 img-fluid" alt="...">
                                @else
                                    @foreach ($announcement->images as $image)
                                    <div class="carousel-item {{$loop->first ? 'active' : ''}}">
                                        <img class="card-img-top" src="{{ $image->src }}">
                                        <button type="button" class="btn btn-primary">
                                  Adult <span class="badge badge-light {{ $image->adult}}"><i class="fas fa-circle"></i></span>
                                </button>
                                <button type="button" class="btn btn-primary">
                                  Spoof <span class="badge badge-light {{ $image->spoof}}"><i class="fas fa-circle"></i></span>
                                </button>
                                <button type="button" class="btn btn-primary">
                                  Medical <span class="badge badge-light {{ $image->medical}}"><i class="fas fa-circle"></i></span>
                                </button>
                                <button type="button" class="btn btn-primary">
                                  Violence <span class="badge badge-light {{ $image->violence}}"><i class="fas fa-circle"></i></span>
                                </button>
                                <button type="button" class="btn btn-primary">
                                  Racy <span class="badge badge-light {{ $image->racy}}"><i class="fas fa-circle"></i></span>
                                </button>
                                    </div> 

                                    @endforeach
                                @endif
                              </div>
                              <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                              </a>
                              <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                              </a>
                            </div>
                            <div class="card-body text-center">
                                <h5 class="card-title">{{ $announcement->title }}</h5>
                            <p class="card-text">{{ $announcement->description }}</p>
                        </div>
                        <div class="card-footer bg-transparent border-dark">
                          <h5 class="text-info">Prezzo: € {{ $announcement->price }}</h5>
                      </div>  
                      <div class="card-footer bg-transparent border-dark">  
                          <h5 class="text-info">Categoria: {{ $announcement->category->name }}</h5>
                      </div>
                      <div class="card-footer bg-transparent border-dark">                  
                          <h5 class="text-info">Annuncio inserito da: {{ $announcement->user->name }}</h5>
                      </div>

                      <div class="d-flex justify-content-center">
                          <form action="{{ route('revisor.accept', [ $announcement->id ] ) }}" method="POST">
                            @csrf
                            <button style="margin: 10px;" class="btn btn-success" type="submit">Accetta</button>
                        </form>
                        &nbsp &nbsp &nbsp
                        <form action="{{ route('revisor.reject', [ $announcement->id ] ) }}" method="POST">
                            @csrf
                            <button style="margin: 10px;" class="btn btn-danger" type="submit">Rifiuta</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>  
    </div>  
</div>  
</div> 
</div>   

</div>   
</div>  
<br>



@endforeach

@else
<div class="container justify-content-center">
   <div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card justify-content-center">
        <div class="card-header text-center">
        <h1>Non ci sono annunci da revisionare</h1>
        </div>
        
        </div>
    </div>
   </div>
</div>

@endif

<br>

<div class="row justify-content-center">
  @include('return_home')
</div>

@endsection



