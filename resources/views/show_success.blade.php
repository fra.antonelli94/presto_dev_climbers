@if(session('success') == 'announcement.stored')
	<div class='alert alert-success'>
		<h2>Hai inserito correttamente il tuo annuncio</h2>
		<p>Il nostro team di revisione controllerà il tuo annuncio e lo approverà</p>
	</div>			
@endif	

@if(session('success') == 'announcement.edited')
	<div class='alert alert-success'>
		<h2>Modifiche effettuate con successo!</h2>
	</div>			
@endif	