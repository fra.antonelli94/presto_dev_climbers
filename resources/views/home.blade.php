@extends('layouts.app')

@section('redirect')
<meta http-equiv="refresh" content="10;URL=/">
@endsection

@section('content')
<div class="container justify-content-center">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card justify-content-center">
                <div class="card-header">Dashboard</div>

                <div class="card-body justify-content-center">
                    @include('show_success')

                    <br>

                    <h2>Crea un nuovo annuncio o tra 10 secondi verrai reindirizzato alla home</h2>
                    @include('announcement_create')
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
