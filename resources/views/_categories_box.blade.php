<div class="row">
    <div class="col-md-auto col-lg-auto col-sm-auto">
        <div class="row">
        @foreach ($categories as $category)
            <div class="col-md-4">
                <div class="box @if(isSet($category_id) && $category->id == $category_id) currentCategory @endif">
                    <a href="{{route('announcement.category', [$category->id])}}" class="my-a">
                    <i class="fas {{strtolower($category->name)}}"></i>
                    <h6>{{$category->name}}</h6>
                    </a>
                </div>
            </div>
        @endforeach
        </div>
    </div>
</div>