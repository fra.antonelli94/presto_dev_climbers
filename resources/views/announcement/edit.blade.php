@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="text-center bg-light">
                    <h1 style="font-family: verdana">Modifica Annuncio</h1>
                    <div class="card-body">

                        @include('show_errors')
                        @include('show_success')


                        <form method="POST" action="{{ route('announcement.update', [$announcement->id]) }}">
                            <input type="hidden" name="_method" value="PUT">
                        @csrf

                        <div>
                        <b-card border-variant="info" header="Titolo Annuncio" align="center"  header-bg-variant="info">
                            <input type="text" name="title" value="{{old('title', $announcement->title)}}" class="form-control" id="title">
                        </b-card>
                        </div>

                        <br>

                        <div class="form-group">
                            <b-card border-variant="info" header="Descrizione" align="center"  header-bg-variant="info">
                            <textarea name="description" cols="100" rows="10" class="form-control" id="description">{{ old('description', $announcement->description )}}</textarea>
                            </b-card>
                        </div>

                        <div class="form-group">
                         <b-card border-variant="info" header="Categoria" align="center"  header-bg-variant="info">
                        
                         <select class="from-control" id="categories" name="category">
                        @foreach ($categories as $category)
                         <option  value="{{$category->id}}" @if($category->id == $announcement->category->id) selected @endif>{{ $category->name }}</option>
                        @endforeach
                        </select>
                        </b-card>
                        </div>

                        <br>

                        <div class="form-group">
                         <b-card border-variant="info" header="Prezzo" align="center"  header-bg-variant="info">
                            <input type="number" name="price"value="{{old('price', $announcement->price)}}" class="form-control" id="price">
                         </b-card>   
                        </div>
                        
                        

                        <br>
                        <h2>Immagini</h2>
                        <div class="dropzone" id="drophere"></div>


                        <br>
                          <b-button variant="success" type="submit">Salva Annuncio Modificato</b-button>
                        <br>

                        </form>
                    </div>
                </div> 
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
    $(function () {
            var myDropzone = new Dropzone("div#drophere", {
                url: "{{ route('announcement.store.image', [$announcement->id]) }}",
                params: {
                    _token: Laravel.csrfToken
                },

                addRemoveLinks: true,

                init: function() {
                    theDropzoneSelf = this; 
                    
                    $.get("{{ route('announcement.get.images', $announcement->id)}}", function(data) {                      

                        console.log('dati ', data);

                        $.each(data, function(key,value) {
                          
                          var mockFile = { 
                            serverId: value.id, 
                            size: value.size 
                          };

                          var re = /(?:\.([^.]+))?$/;
                          var ext = re.exec(value.src)[1];
                          var extension = "." + ext;
                          var src = value.src.slice(0, -1 * extension.length);
                          var thumbnail_img = src + "-image(120x120-crop)" + extension;

                          theDropzoneSelf.options.addedfile.call(theDropzoneSelf, mockFile);
                          theDropzoneSelf.options.thumbnail.call(theDropzoneSelf, mockFile, thumbnail_img);

                        });
                      
                    });
                  }
            });

            myDropzone.on("success", function (file, response) {
                file.serverId = response.id; 
            });

            myDropzone.on("removedfile", function(file) {
                  var id = file.serverId;        
                  $.ajax({
                    type: 'DELETE',
                    url: "{{ route('announcement.delete.image', [$announcement->id, ""]) }}" + "/" + id,
                    data: "_token=" + Laravel.csrfToken,
                    dataType: 'html'
                  });
            });
        });
</script>
@endsection
