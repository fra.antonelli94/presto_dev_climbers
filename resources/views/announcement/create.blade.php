@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="text-center bg-light">
                    <h1 style="font-family: verdana">Inserisci Annuncio</h1>
                    <div class="card-body">

                        @include('show_errors')

                        <form method="POST" action="{{ route('announcement.store') }}">
                        	
                        @csrf

                        <div>
                        <b-card border-variant="info" header="Titolo Annuncio" align="center"  header-bg-variant="info">
                            <input type="text" name="title" value="{{old('title')}}" class="form-control" id="title">
                        </b-card>
                        </div>

                        <br>

                        <div class="form-group">
                            <b-card border-variant="info" header="Descrizione" align="center"  header-bg-variant="info">
                            <textarea name="description" cols="100" rows="5" value="{{old('description')}}" class="form-control" id="description"></textarea>
                            </b-card>
                        </div>

                        <div>
                         <b-card border-variant="info" header="Categoria" align="center"  header-bg-variant="info">
                         <select class="from-control" id="categories" name="category">
                        @foreach ($categories as $category)
                         <option  value="{{$category->id}}">{{ $category->name }}</option>
                        @endforeach
                        </select>
                        </b-card>

                        <br>



                        <div class="form-group">
                         <b-card border-variant="info" header="Prezzo" align="center"  header-bg-variant="info">
                            <input type="number" name="price"value="{{old('price')}}" class="form-control" id="price">
                        </div>
                        </b-card>

                        <div class="row justify-content-center">
                            @include('return_home') <hr>
                          <b-button variant="success" type="submit">Inserisci Annuncio!</b-button>
                      </div> 

                        </form>

                    </div>
                </div> 
            </div>
        </div>
    </div>
</div>
@endsection
