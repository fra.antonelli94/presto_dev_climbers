@extends('layouts.app')
@section('content')
@include('_categories_box')

<div class="row justify-content-center">
  @include('announcement_create')
</div>

<br>

<div class="row justify-content-center">
  @include('return_home')
</div>



<div class="row justify-content-center" style="background-color: #e9ecef; padding: 4rem 2rem">
    <div class="col-lg-10 col-md-10 col-sm-10">
      <h1 class="text-center">Ecco tutti gli annunci:</h1>
        <div class="card-deck">
            @foreach($announcements as $announcement)
            <div class="col-md-4">
                <br>
              <a href="{{route('announcement.detail', [$announcement->id])}}">
              <div class="card border-dark animated slideInUp">
              @if(count($announcement->images) == 0)
                <img class="card-img-top" src="/img/image_not_found.png">
              @else
                <img class="card-img-top" src="{{ $announcement->images->first()->src }}">
                <br>
              @endif
              </a>
                <div class="card-body">
                  <a href="{{route('announcement.detail', [$announcement->id])}}">
                    <h5 class="card-title">{{ $announcement->title }}</h5>
                  </a>
                  <p class="card-text">{{ $announcement->description }}</p>
                </div>
                <div class="card-footer bg-transparent border-dark">
                  <h5 class="text-muted">Prezzo: € {{ $announcement->price }}</h5>
                </div>  
                <div class="card-footer bg-transparent border-dark">  
                  <h5 class="text-muted">Categoria: <a href="{{ route ('announcement.category',[$announcement->category->id] )}}"> {{ $announcement->category->name }}</a></h5>
                </div>
                <div class="card-footer bg-transparent border-dark">                  
                  <h5 class="text-muted">Annuncio inserito da: {{ $announcement->user->name }}</h5>
                </div>
              </div>
            </div>
          @endforeach
        </div>

        <div class="mt-3 d-flex justify-content-center">
            {{ $announcements->links() }}
        </div>
    </div>  
</div> 

@endsection