@extends('layouts.app')
@section('content')



<div class="row justify-content-center">
	<div class="col-lg-8 col-md-12 col-sm-8">
			<div class="card-header text-center"><h2>Di seguito gli annunci della categoria: {{ $category->name}}</h2>

		<div class="card-deck">

				@foreach ($category->announcements as $announcement)

				<div class="col-md-4">
				   <br>
					<a href="{{route('announcement.detail', [$announcement->id])}}">
		              <div class="card border-dark animated slideInUp">
		              @if(count($announcement->images) == 0)
		                <img class="card-img-top" src="/img/image_not_found.png">
		              @else
		                <img class="card-img-top" src="{{ $announcement->images->first()->src }}">
		                <br>
		              @endif
	              	</a>
						<div class="card-body">
							<a href="{{route('announcement.detail', [$announcement->id])}}">
								<h5 class="card-title">{{ $announcement->title }}</h5>
							</a>
							<p class="card-text">{{ $announcement->description }}</p>
						</div>
						<div class="card-footer bg-transparent border-dark">
							<h5 class="text-muted">Prezzo: € {{ $announcement->price }}</h5>
						</div>  
						<div class="card-footer bg-transparent border-dark"> 
								<h5 class="text-muted">Categoria: {{ $announcement->category->name }}</h5>
						</div>
						<div class="card-footer bg-transparent border-dark">                  
							<h5 class="text-muted">Annuncio inserito da: {{ $announcement->user->name }}</h5>
						</div>
					</div>
				</div>
			

				@endforeach

			</div>
		</div>
	</div>
</div>

@endsection