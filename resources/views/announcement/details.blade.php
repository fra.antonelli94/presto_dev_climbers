@extends('layouts.app')
@section('content')
<div class="row justify-content-center col-lg-12 col-md-8 col-sm-8">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header text-center"><h1>{{ $announcement->title}}</h1></div>
            <div class="card-body">
                <div class="announcement">
                    <div class="row">
                        <div class="col-md-12">
                           <div id="carouselExampleIndicators" class="carousel slide w-75 m-auto" data-ride="carousel">
                              <div class="carousel-inner">
                                @if(count($announcement->images) == 0)
                                  <img src="/img/image_not_found.png" class="d-block img-fluid" alt="...">
                                  @else
                                    @foreach ($announcement->images as $image)
                                    <div class="carousel-item {{$loop->first ? 'active' : ''}}">
                                        <img class="card-img-top img-fluid"  src="{{ $image->src }}">
                                    </div> 
                                    @endforeach
                                @endif
                              </div>
                                  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                  </a>
                                  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                  </a>
                            </div>
                        </div>
                    </div>

                    <br><br>

                        <div class="card text-center col-sm-12">
                          <div class="card-header">
                            <h1>Descrizione:</h1>
                          </div>
                              <div class="card-body">
                                <h5 class="card-text">{{ $announcement->description}}</h5>
                              </div>
                          <div class="card-footer text-primary">
                            <p style="font-size: 18px;">Annuncio pubblicato da: {{$announcement->user->name}}</p> <hr> <p style="font-size: 18px;">Categoria: {{ $announcement->category->name}}</p> <hr> <p style="font-size: 18px;">Prezzo: € {{ $announcement->price}}</p>
                          </div>
                        </div>                    

                    <hr>

                    <div class="card-body d-flex justify-content-center"> 
                    @include('return_home') <hr>
                        <button class="btn btn-success">
                        <a href="mailto:{{ $announcement->user->email }}" style="color: black;">Contatta il venditore</a>
                        </button>
                    </div>
                </div>
            </div>
            </div>
        </div>

    </div>


@endsection

