@extends('layouts.app')
@section('content')

<div class="row jumbotron justify-content-center">  
  <div class="col-md-6 d-none d-md-block text-center animated slideInDown " style="background-color: none;">
      @include("_categories_box")
      @include('show_announcement')
  </div>  
  <div class="col-sm-6 text-center animated slideInDown" style="background-color: none;">
    <h1 class="display-4 animated slideInLeft" style="font-family: 'Inconsolata', monospace;">Benvenuto su</h1>
          <img src="/img/presto_logo.png" class="img-fluid">
<h3>INSERISCI IL TUO ANNUNCIO IN MENO DI 2 MINUTI!</h3>
      <br>
      @include('announcement_create')
  </div>  
</div>

<br><br>

<div class="row justify-content-center" style="background-color: #e9ecef; padding: 4rem 2rem">
    <div class="col-lg-10 col-md-10 col-sm-10">
      @if(count($announcements) > 0)
      <h2 class="text-center">Ecco gli ultimi 6 annunci inseriti:</h2>
      @else
      <h2 class="text-center">Al momento non ci sono annunci disponibili.</h2>
      @endif
        <div class="card-deck">
            @foreach($announcements as $announcement)
            <div class="col-lg-4 col-md-6 col-sm-12">
                <br>
              <div class="card border-dark animated slideInUp">
              <a href="{{route('announcement.detail', [$announcement->id])}}">

              <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                  @if(count($announcement->images) == 0)
                    <img src="/img/image_not_found.png" class="d-block w-50 img-fluid" alt="...">
                  @else
                      @foreach ($announcement->images as $image)
                      <div class="carousel-item {{$loop->first ? 'active' : ''}}">
                          <img class="card-img-top" src="{{ $image->src }}">
                      </div> 
                      @endforeach
                  @endif
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                </a>
              </div>
              </a>
                <div class="card-body">
                  <a href="{{route('announcement.detail', [$announcement->id])}}">
                    <h5 class="card-title text-center">{{ $announcement->title }}</h5>
                  </a>
                  <p class="card-text text-center">{{ $announcement->description }}</p>
                </div>
                <div class="card-footer bg-transparent border-dark">
                  <h5 class="text-muted">Prezzo: € {{ $announcement->price }}</h5>
                </div>  
                <div class="card-footer bg-transparent border-dark">  
                  <h5 class="text-muted">Categoria: <a href="{{ route ('announcement.category',[$announcement->category->id] )}}"> {{ $announcement->category->name }}</a></h5>
                </div>
                <div class="card-footer bg-transparent border-dark">                  
                  <h5 class="text-muted">Annuncio inserito da: {{ $announcement->user->name }}</h5>
                </div>
              </div>
            </div>
          @endforeach
        </div>
    </div>  
</div>  
  
@endsection



