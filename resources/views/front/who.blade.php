@extends('layouts.app')
@section('content')

<style type="text/css">
	.grid-container {
	  display: grid;
	  grid-column-gap: 50px;
	}

</style>

<div class="container">
	<div class="row">
		<div class="col-md-3">
			<div class="card" >
			  <img src="https://via.placeholder.com/150" class="card-img-top" alt="...">
			  <div class="card-body">
			    <h5 class="card-title">Francesco Antonelli</h5>
			    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
			    <a href="#" class="btn btn-primary">Go somewhere</a>
			  </div>
			</div>
		</div>

		<div class="col-md-3">
			<div class="card" >
			  <img src="https://via.placeholder.com/150" class="card-img-top" alt="...">
			  <div class="card-body">
			    <h5 class="card-title">Sonia Intonti</h5>
			    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
			    <a href="#" class="btn btn-primary">Go somewhere</a>
			  </div>
			</div>
		</div>

		<div class="col-md-3">
			<div class="card" >
			  <img src="https://via.placeholder.com/150" class="card-img-top" alt="...">
			  <div class="card-body">
			    <h5 class="card-title">Fabrizio Lopez</h5>
			    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
			    <a href="#" class="btn btn-primary">Go somewhere</a>
			  </div>
			</div>
		</div>

		<div class="col-md-3">
			<div class="card" >
			  <img src="https://via.placeholder.com/150" class="card-img-top" alt="...">
			  <div class="card-body">
			    <h5 class="card-title">Giuseppe Tarantino</h5>
			    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
			    <a href="#" class="btn btn-primary">Go somewhere</a>
			  </div>
			</div>
		</div>
	</div>
</div>


@endsection