@extends('layouts.app')



@section('content')

<div class="container justify-content-center">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card justify-content-center">
                <div class="card-header text-center"><h1>Lavora con noi</h1> </div>

                <div class="card-body justify-content-center text-center">
                    <form method="POST" name="contact_form" action="{{route('contacts.workwithusok')}}">
                    @csrf
                    <p style="color:black; font-size: 18px;">Inserisci nome e cognome :</p>
                    <input class="form-control" type="text" name="name" value="{{Auth::user()->name}}">
                    <br>
                    
                    <p style="color:black; font-size: 18px;">Inserisci indirizzo e-mail :</p>
                    <input class="form-control" type="text" name="email" value="{{Auth::user()->email}}">

                    <br>
                    <p style="color:black; font-size: 18px;">Scrivici perchè vuoi lavorare con noi:</p>
                    <input class="form-control" type="text" name="description">

                    <br>
                    <p style="color:black; font-size: 18px;">Allega il tuo curriculum :</p>
                    <input type="file" class="form-control" id="image" name="cv"  required>  
                    <hr>

                    <button class="btn btn-success" variant="success" type="submit">Invia richiesta di lavoro </button>
                </form>

            </div>


            <br>
        </div>
    </div>
</div>
</div>


@endsection
